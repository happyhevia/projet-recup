<?php

namespace AppBundle\Repository;

/**
 * EvenementRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EvenementRepository extends \Doctrine\ORM\EntityRepository
{
	public function getAllEvents(){
		return $this->getEntityManager()
					->createQuery('
						SELECT e FROM AppBundle:Evenement e ORDER BY e.day DESC ')
					->getResult();
	}
	public function getAllEventsByCreated(){
		return $this->getEntityManager()
					->createQuery('
						SELECT e FROM AppBundle:Evenement e ORDER BY e.created DESC ')
					->getResult();
	}
	
	
}
